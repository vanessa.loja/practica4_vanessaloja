'use strict';
var models = require('../models/');
var marca = models.marca;

class MarcaController{
    async listar(req,res){
        var lista = await marca.findAll({
            attributes: ['nombre','estado','external_id']
        });
        res.json({msg: "OK!",code: 200, info: lista});
    }
}

module.exports = MarcaController;