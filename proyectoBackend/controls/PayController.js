const https = require('https');
const querystring = require('querystring');
const { validationResult } = require('express-validator');

class PayController {
  async procesarPago(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        res.status(400).json({ errors: errors.array() });
        return;
      }

      const path = '/v1/checkouts';
      const data = querystring.stringify({
        'entityId': '8a8294175d602369015d73bf009f1808',
        amount: req.body.monto,
        'currency': 'USD',
        'paymentType': 'DB'
      });

      const options = {
        port: 443,
        host: 'eu-test.oppwa.com',
        path: path,
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': data.length,
          'Authorization': 'Bearer OGE4Mjk0MTc1ZDYwMjM2OTAxNWQ3M2JmMDBlNTE4MGN8ZE1xNU1hVEQ1cg=='
        }
      };

      const response = await new Promise((resolve, reject) => {
        const postRequest = https.request(options, function (res) {
          const buf = [];
          res.on('data', chunk => {
            buf.push(Buffer.from(chunk));
          });
          res.on('end', () => {
            const jsonString = Buffer.concat(buf).toString('utf8');
            try {
              resolve(JSON.parse(jsonString));
            } catch (error) {
              reject(error);
            }
          });
        });

        postRequest.on('error', reject);
        postRequest.write(data);
        postRequest.end();
      });

      // Guarda el ID del checkout en la variable de sesión
      req.session.checkoutId = response.id;

      res.json(response);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  

  async generarFormularioPago(req, res) {
    try {
      const checkoutId = req.session.checkoutId; // Obtén el checkoutId de la variable de sesión

      // Genera el contenido HTML del formulario de pago
      const paymentFormHtml = `
        <!DOCTYPE html>
        <html>
        <head>
          <script src="https://eu-test.oppwa.com/v1/paymentWidgets.js?checkoutId=${checkoutId}"></script>
        </head>
        <body>
          <form action="https://flutter.dev" class="paymentWidgets" data-brands="AMEX MASTER UNIONPAY VISA"></form>
        </body>
        </html>
      `;

      res.send(paymentFormHtml);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async verificarPago(req, res) {
    try {
      const checkoutId = req.session.checkoutId; // Obtén el ID del checkout de la variable de sesión

      const path = `/v1/checkouts/${checkoutId}/payment?entityId=8a8294175d602369015d73bf009f1808`;
      const options = {
        port: 443,
        host: 'eu-test.oppwa.com',
        path: path,
        method: 'GET',
        headers: {
          'Authorization': 'Bearer OGE4Mjk0MTc1ZDYwMjM2OTAxNWQ3M2JmMDBlNTE4MGN8ZE1xNU1hVEQ1cg=='
        }
      };

      const response = await new Promise((resolve, reject) => {
        const getRequest = https.request(options, function (res) {
          const buf = [];
          res.on('data', chunk => {
            buf.push(Buffer.from(chunk));
          });
          res.on('end', () => {
            const jsonString = Buffer.concat(buf).toString('utf8');
            try {
              resolve(JSON.parse(jsonString));
            } catch (error) {
              reject(error);
            }
          });
        });

        getRequest.on('error', reject);
        getRequest.end();
      });

      res.json(response);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  
}

module.exports = PayController;
