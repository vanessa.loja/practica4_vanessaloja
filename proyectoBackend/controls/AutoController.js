'use strict';
const { body, validationResult, check } = require('express-validator');
const { Op, sequelize } = require('sequelize');
var models = require('../models/');
var persona = models.persona;
var venta = models.venta;
var auto = models.auto;
var marca = models.marca;
const bcrypt = require('bcrypt');
const salRounds = 8;
class AutoController {
    async listarSinDuenio(req, res) {
        try {
            const listar = await auto.findAll({
                attributes: ['modelo', 'anio', 'cilindraje', 'color', 'placa', 'vendido', 'precio', 'external_id', "imagen"],
                where: {
                    '$venta.id$': null
                },
                include: [
                    {
                        model: venta,
                        attributes: [],
                        required: false
                    }
                ],
            })
            console.log("holaaaa2", listar);
            res.status(200);
            console.log("holaaaa", listar);
            res.json({ msg: "0k", code: 200, info: listar });
        } catch (error) {
            console.error(error);
            console.log("holaaaa3", listar);
            res.status(500).json({ message: 'Error al obtener los autos disponibles' });
        }
    }

    async contar(req, res) {
        var listar = await auto.count();
        res.status(200);
        res.json({ msg: "Obtenido numero de autos!", code: 200, info: listar });

    }

    async obtener(req, res) {
        const external = req.params.external;
        try {
            findAll
            var lista = await auto.findOne({
                where: { external_id: external },
                attributes: ['modelo', 'anio', 'cilindraje', 'color', 'placa', 'vendido', 'precio', 'external_id', 'imagen']
            });
            if (lista == null) {
                lista = {
                    msg: "Sin auto"
                };
            }
            res.status(200);
            res.json({ msg: "OK!", code: 200, info: lista });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener el auto solicitado' });
        }
    }

    async listarConDuenio(req, res) {
        try {
            const listar = await auto.findAll({
                attributes: ['modelo', 'anio', 'cilindraje', 'color', 'placa', 'vendido', 'precio', 'external_id'],
                where: {
                    '$venta.id$': {
                        [Op.not]: null
                    }
                },
                include: [
                    {
                        model: venta,
                        attributes: ['id_persona'],
                        required: true
                    }
                ],
            })
            res.status(200);
            res.json({ msg: "0k", code: 200, info: listar });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener los autos disponibles' });
        }
    }


    /*async guardar(req, res) {
        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400);
            return res.json({ msg: "Datos faltantes", code: 400, errors: errors.array() });
        }

        const marcaId = req.body.external_marca;
        if (!marcaId) {
            res.status(400);
            return res.json({ msg: "Faltan datos: external_marca no proporcionado", code: 400 });
        }

        try {
            let marcaAux = await marca.findOne({ where: { external_id: marcaId } });
            if (marcaAux === null) {
                res.status(400);
                return res.json({ msg: "Datos no encontrados: marca no existe", code: 400 });
            }

            var data = {
                modelo: req.body.modelo,
                color: req.body.color,
                anio: req.body.anio,
                cilindraje: req.body.cilindraje,
                placa: req.body.placa,
                motor: req.body.motor,
                vendido: req.body.vendido,
                precio: req.body.precio,
                imagen: req.file.myImage, // Cambio aquí: usar req.file.myImage
                id_marca: marcaAux.id
            };

            await auto.create(data);
            res.json({ msg: "Se han registrado los datos", code: 200 });
        } catch (error) {
            res.status(500);
            res.json({ msg: error.message, code: 500 });
        }
    }*/

    async guardar(req, res) {
        let errors = validationResult(req);
        console.log("ARCHIVO DE IMAGEN", req.file);

        if (errors.isEmpty()) {
            var marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
                console.log(marcaAux);
                if (marcaAux) {
                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        modelo: req.body.modelo,
                        color: req.body.color,
                        anio: req.body.anio,
                        cilindraje: req.body.cilindraje,
                        placa: req.body.placa,
                        motor: req.body.motor,
                        vendido: req.body.vendido,
                        precio: req.body.precio,
                        imagen: req.file.filename, // Aquí guardamos el nombre de la imagen
                        id_marca: marcaAux.id
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await auto.create(data, { transaction });
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }




    /*async guardar(req, res) {
        
            let errors = validationResult(req);
            if (errors.isEmpty()) {
                const marcaId= req.body.external_marca;

                if (1===1) {
                    let marcaAux = await marca.findOne({ where: { external_id: marcaId } });
                    if (marcaAux) {
                        var data = {
                            modelo: req.body.modelo,
                            color: req.body.color,
                            anio: req.body.anio,
                            cilindraje: req.body.cilindraje,
                            placa: req.body.placa,
                            motor: req.body.motor,
                            vendido: req.body.vendido,
                            precio: req.body.precio,
                            imagen: req.file. imagen, 
                            id_marca: marcaAux.id
                        }
                        try {
                            await auto.create(data);
                            res.json({ msg: "Se han registrado los datos", code: 200 });
                        } catch (error) {
                            if (error.errors && error.errors[0].message) {
                                res.status(400);
                                res.json({ msg: error.errors[0].message, code: 400 });
                            } else {
                                res.status(400);
                                res.json({ msg: error.message, code: 400 });
                            }
                        }
                    } else {
                        res.status(400);
                        res.json({ msg: "Datos no encontrados", code: 400 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Faltan datos", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos faltantes", code: 400, errors: errors });
            }
    }*/


    async modificar(req, res) {
        if (!req.body.external_auto) {
            req.body.external_auto = "INDEFINIDO"
        }
        var autom = await auto.findOne({ where: { external_id: req.body.external_auto } });
        if (autom === null) {
            res.json({ msg: "No existe el registro de auto", code: 400 });
        } else {
            autom.modelo = req.body.modelo;
            autom.color = req.body.color;
            autom.anio = req.body.anio;
            autom.cilindraje = req.body.cilindraje;
            autom.placa = req.body.placa;
            autom.vendido = req.body.vendido;
            var result = await autom.save();
            if (result === null) {

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }

}
module.exports = AutoController;