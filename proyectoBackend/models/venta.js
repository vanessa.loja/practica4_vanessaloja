'use strict';
module.exports = (sequalize, DataTypes) => {
    const venta = sequalize.define('venta', {

        external_id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
          },
        
        precio_unitario: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        
        iva: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        
        precio_total: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },

    }, {freezeTableName: true});
    venta.associate = function(models) {
        venta.belongsTo(models.auto, {foreignKey: 'id_auto'});
        venta.belongsTo(models.persona, {foreignKey: 'id_persona'});
        venta.hasMany(models.factura, { foreignKey: 'id_venta' });
    };
    return venta;
}