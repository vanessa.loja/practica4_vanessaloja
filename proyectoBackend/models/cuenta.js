'use strict';
module.exports = (sequalize, DataTypes) => {
    const cuenta = sequalize.define('cuenta', {

        username: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        clave: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true},
        correo: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {freezeTableName: true});
    cuenta.associate = function(models) {
        cuenta.belongsTo(models.persona, {foreignKey: 'personaId'});
    }
    return cuenta;
}