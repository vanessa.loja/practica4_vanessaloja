'use strict';
module.exports = (sequalize, DataTypes) => {
    const persona = sequalize.define('persona', {
        nombres: {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        apellidos: {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        identificacion: {
            type: DataTypes.STRING(20),
            unique: true,
            allowNull: false,
            defaultValue: "Sin_Datos"
        },
        tipo_identificacion: {
            type: DataTypes.ENUM("CEDULA","PASAPORTE","RUC"),
            unique: true,
            allowNull: false,
            defaultValue: "CEDULA"
        }
        ,
        direccion: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: "Sin_Datos"
        },
        external_id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4},
        estado: {
            type: DataTypes.BOOLEAN, 
            defaultValue: true
        }
    }, {freezeTableName: true});
    persona.associate = function(models) {
        persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
        persona.hasOne(models.cuenta, {foreignKey: 'personaId',as:'cuenta', unique: true});
        persona.hasMany(models.venta, { foreignKey: 'personaId'});
    }
    return persona;
}