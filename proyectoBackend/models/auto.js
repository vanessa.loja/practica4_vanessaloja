'use strict';
module.exports = (sequalize, DataTypes) => {
    const auto = sequalize.define('auto', {
        modelo: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        anio: { type: DataTypes.STRING(12), defaultValue: "NO_DATA" },
        cilindraje: { type: DataTypes.STRING(12), defaultValue: "NO_DATA" },
        color: { type: DataTypes.ENUM("BLANCO","ROJO","NEGRO","AZUL","VERDE","MORADO","PLATA","MULTICOLOR"), defaultValue: "BLANCO"},
        placa: { type: DataTypes.STRING(255), defaultValue: "NO_DATA"},
        precio: { type: DataTypes.DOUBLE(), defaultValue: 0},
        vendido: {type: DataTypes.BOOLEAN, defaultValue: false},
        estado:{ type: DataTypes.BOOLEAN, defaultValue: true},
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        imagen: { type: DataTypes.STRING(255), defaultValue: "NO_DATA" },
        
    }, {freezeTableName: true});
    auto.associate = function(models) {
        auto.hasMany(models.venta, { foreignKey: 'id_auto' });
        auto.belongsTo(models.marca, {foreignKey: 'id_marca'});
      };
    return auto;
}