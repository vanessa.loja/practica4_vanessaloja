import 'package:auto/servicios/modelo/RespuestaGenerica.dart';

/*class InicioSesionWS extends RespuestaGenerica{
  String token = '';
  String username = '';
  String correo = '';
  InicioSesionWS({this.token='', this.correo='', this.username='' });
  InicioSesionWS.fromMap(Map<String, dynamic> mapa, int code, String msg){
  //InicioSesionWS.fromMap(Map<dynamic, dynamic> mapa, int code){
    if(mapa.isEmpty){
      token = "";
      username = "";
      correo = "";
    }else{
      token = mapa["token"];
      username = mapa["username"];
      correo = mapa["correo"];
    }
    this.msg = msg;
    this.code = code;
  }
}*/
class InicioSesionWS extends RespuestaGenerica{
  String token = '';
  String username = '';
  String correo = '';

  InicioSesionWS({this.token='', this.correo='', this.username='' });

  InicioSesionWS.fromMap(Map<dynamic, dynamic> mapa){
    if(mapa.isEmpty){
      token = "";
      username = "";
      correo = "";
    } else {
      token = mapa["token"];
      username = mapa["username"];
      correo = mapa["correo"];
      code = mapa["code"]; // Asignar directamente el valor de 'code'
      msg = mapa["msg"];   // Asignar directamente el valor de 'msg'
    }
  }
}

