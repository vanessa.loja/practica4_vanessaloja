import 'dart:developer';
import 'dart:ffi';

class AutosWS{
  String modelo = '';
  String external_id = '';
  String placa = '';
  String cilindraje = '';
  double precio = 0.0;
  String imagen = '';
  int anio = 0;
  bool vendido = false;
  String marca = '';

  AutosWS();
  AutosWS.fromMap(Map<dynamic, dynamic> mapa){
    modelo = mapa['modelo'];
    external_id = mapa['external_id'];
    imagen = mapa['imagen'];
    placa = mapa['placa'];
    cilindraje = mapa['cilindraje'];
    vendido = (mapa['vendido'].toString() == 'true') ? true : false;
    precio = double.parse(mapa['precio'].toString());
    anio = int.parse(mapa['anio'].toString());
    marca = mapa['marca']['nombre'];
  }
}