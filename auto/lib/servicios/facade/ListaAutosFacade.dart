import 'dart:convert';
import 'dart:developer';

import 'package:auto/servicios/conexion.dart';
import 'package:auto/servicios/facade/modelo/AutosWS.dart';
import 'package:auto/servicios/facade/modelo/ListaAutosWS.dart';
import 'package:auto/servicios/modelo/RespuestaGenerica.dart';
import 'package:auto/utiles/Utilidades.dart';

class ListaAutosFacade{
  conexion _conn = new conexion();
  Utilidades _util = new Utilidades();
  Future<ListaAutoWS> listaAuto()async{
    log("ListaAutoFacade");
    String token = await _util.getValue("token") as String? ?? "";

    //String token = await _util.getValue("token") as String;
    var response = await _conn.solicitudGet('auto', token);

    return _response((response.code != 0) ? response.info : null);
  }
}

ListaAutoWS _response(dynamic data){
  var sesion = ListaAutoWS();
  if(data != null){
    Map<String, dynamic> mapa = jsonDecode(data);

    if(mapa.containsKey("info")){
      List datos = jsonDecode(jsonEncode(mapa["info"]));
      sesion = ListaAutoWS.fromMap(datos, mapa["msg"], int.parse(mapa["code"].toString()));
    }else{
      List myList = List.empty();
      sesion = ListaAutoWS.fromMap(myList, mapa["msg"], int.parse(mapa["code"].toString()));
    }
  }
  return sesion;
}