import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:auto/servicios/modelo/RespuestaGenerica.dart';
class conexion {
  final String NAME = "conexion";
  final String URL = "http://192.168.48.108:3007";
  final String URL_BASE = "http://192.168.48.108:3007/api/";
  static String NO_TOKEN = "NO_TOKEN";
  static String FILES = "http://192.168.48.108:3007/images/";

  Future<RespuestaGenerica> solicitudPost(String dir_recurso,
      Map<dynamic, dynamic> data,
      String token) async {
    log("${this.NAME}:Solicitud en POST");
    Map<String, String> _header = {'Content-Type':'application/json'};
    if(token != NO_TOKEN){
      _header = {'Content-Type':'application/json', 'x-api-token': token};
    }
    final String url = URL_BASE+dir_recurso;
    log("${this.NAME}: Solicitud en POST"+url);
    final uri = Uri.parse(url);
    try{
      final response = await http.post(uri, headers: _header, body: jsonEncode(data));
      log(response.body);
      log(response.statusCode.toString());
      if(response.statusCode != 200){
        return _responseJson(0, response.body, "No data");
      }else{
        return _responseJson(200, response.body, "OK");
      }
    }catch(e){
      Map<dynamic, dynamic> mapa = {"info": e.toString()};
      return _responseJson(0, mapa, "Hubo un error");
    }
  }
  RespuestaGenerica _responseJson(int code, dynamic data, String msg){
    var respuesta = RespuestaGenerica();
    respuesta.code = code;
    respuesta.msg = msg;
    if (data is String) {
      respuesta.info = jsonDecode(data);
    } else {
      respuesta.info = data;
    }
    return respuesta;
  }


  Future<RespuestaGenerica> solicitudGet(String dir_recurso,String token) async {
    log("${this.NAME}: Solicitud en GET");
    Map<String, String> _header = {'Content-Type':'application/json'};
    if(token != NO_TOKEN){
      _header = {'Content-Type':'application/json', 'x-api-token': token};
    }
    final String url = URL_BASE+dir_recurso;
    final uri = Uri.parse(url);
    try {
      final response = await http.get(uri, headers: _header);
      if(response.statusCode != 200){
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return _responseJson(0, response.body, "No se encontraron los datos!!!!");
        //return _responseJson(int.parse(mapa['code']. toString()), response.body, mapa['msg'].toString());
      }else{
        return _responseJson(200, response.body, "OK");
      }
    }catch(e){
      Map<dynamic, dynamic> mapa = {"info": e.toString()};
      return _responseJson(0, mapa, "Hubo un error");
    }
  }



}
