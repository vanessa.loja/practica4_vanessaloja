import 'dart:developer';

import 'package:auto/servicios/conexion.dart';
import 'package:flutter/services.dart';
import 'package:auto/servicios/facade/InicioSesionFacade.dart';
import 'package:auto/servicios/facade/modelo/InicioSesionWS.dart';
import 'package:auto/servicios/utilidades/sessionUtil.dart';
import 'package:auto/vistas/menuPrincipal.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

class registroView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _registroViewState();
  }
}

class _registroViewState extends State<registroView> {
  final _formKey = GlobalKey<FormState>();
  InicioSesionWS sesionWS = new InicioSesionWS();
  String? _selectedDocumentType = 'cedula';
  final TextEditingController ideControl = TextEditingController();
  final TextEditingController apeControl = TextEditingController();
  final TextEditingController nomControl = TextEditingController();
  final TextEditingController dirControl = TextEditingController();
  final TextEditingController correoControl = TextEditingController();
  final TextEditingController claveControl = TextEditingController();
  bool _isObscure = true;

  void _toggleObscure() {
    setState(() {
      _isObscure = !_isObscure;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Scaffold(
            body:
            ListView(
              padding: const EdgeInsets.all(32),
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      "App Autos",
                      style: TextStyle(
                          color: Colors.teal,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'Registrate para obtener nuestro servicio ;)',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: DropdownButtonFormField<String>(
                    value: _selectedDocumentType,
                    items: [
                      DropdownMenuItem(value: 'cedula', child: Text('Cédula')),
                      DropdownMenuItem(value: 'pasaporte', child: Text('Pasaporte')),
                      DropdownMenuItem(value: 'ruc', child: Text('RUC')),
                    ],
                    onChanged: (String? newValue) {
                      setState(() {
                        _selectedDocumentType = newValue;
                      });
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Tipo de documento',
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Seleccione un tipo de documento';
                      }
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: ideControl,
                    keyboardType: TextInputType.number, // Define el teclado numérico
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')), // Permite solo números
                    ],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Identificación',
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Cedula requerida';
                      }
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: nomControl,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nombre',
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nombre requerido';
                      }
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: apeControl,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Apellido',
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Apellido requerida';
                      }
                    },
                  ),
                ),

                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextFormField(
                    controller: dirControl,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Direccion',
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Direccion requerida';
                      }
                    },
                  ),
                ),

                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: correoControl,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Correo',
                      suffixIcon: Icon(Icons.alternate_email),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Correo requerido';
                      }
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    obscureText: _isObscure,
                    controller: claveControl,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Clave',
                      suffixIcon: IconButton(
                        onPressed: _toggleObscure,
                        icon: _isObscure
                            ? Icon(Icons.visibility)
                            : Icon(Icons.visibility_off),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Clave requerida';
                      }
                    },
                  ),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Registrar'),
                      onPressed: () {
                        Navigator.pushNamed(context, '/home');
                      },
                      //print(nameController.text);
                      //print(passwordController.text
                    )),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text('Ya tienes cuenta?'),
                    TextButton(
                      child: const Text(
                        'Iniciar sesion',
                        style: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/home');
                      },
                    )
                  ],
                ),
              ],
            )));
  }
}
