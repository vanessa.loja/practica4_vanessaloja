import 'dart:developer';

import 'package:auto/servicios/conexion.dart';
import 'package:auto/servicios/facade/InicioSesionFacade.dart';
import 'package:auto/servicios/facade/modelo/InicioSesionWS.dart';
import 'package:auto/servicios/utilidades/sessionUtil.dart';
import 'package:auto/utiles/Utilidades.dart';
import 'package:auto/vistas/menuPrincipal.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

class sessionView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _sessionViewState();
  }
}

class _sessionViewState extends State<sessionView> {
  final _formKey = GlobalKey<FormState>();
  InicioSesionWS sesionWS = InicioSesionWS();
  final TextEditingController correoControl = TextEditingController();
  final TextEditingController claveControl = TextEditingController();
  final Utilidades util = Utilidades();
  //final SessionUtil sesionUtil = SessionUtil();
  void _iniciar() async {
    setState(() {
      if (_formKey.currentState!.validate()) {
        log("Boton iniciar presionado 1");
        InicioSesionFacade sesion = InicioSesionFacade();
        //conexion conn = new conexion();
        Map<String, dynamic> mapa = {
          "correo": correoControl.text,
          "clave": claveControl.text
        };
        log(mapa.toString());
        sesion.inicioSesion(mapa).then((value) async {
          log("Aun no entras pipipipipi");
          log('ya envio');
          if (value.token != "") {
            util.saveValue('token', value.token);
            util.saveValue('user', value.username);
            util.saveValue('correo', value.correo); // Agrega esta línea para guardar el correo

            final SnackBar mensaje = SnackBar(
              content: Text('Bienvenido ' + value.username),
            );
            ScaffoldMessenger.of(context).showSnackBar(mensaje);
            Navigator.pushNamed(context, '/menuPrincipal');
          } else {
            final SnackBar mensaje = SnackBar(content: Text(value.msg));
            ScaffoldMessenger.of(context).showSnackBar(mensaje);
          }

        }).catchError((error) {
          log('hay un error');
          log(error.toString());
        });
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    util.getValue("token").then((value){
      if(value != null){
        Navigator.pushNamed(context, '/menuPrincipal');
      }
    });
    return Form(
      key: _formKey,
        child: Scaffold(
    body:
        ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  "App Autos",
                  style: TextStyle(
                      color: Colors.teal,
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                )),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Inicio de sesion',
                  style: TextStyle(fontSize: 20),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: correoControl,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Correo',
                  suffixIcon: Icon(Icons.alternate_email),
                ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Correo requerido';
                    }
                    if (!isEmail(value)) {
                      return 'Debe ser un correo valido';
                    }
                  },
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextFormField(
                obscureText: true,
                controller: claveControl,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Clave',
                ),
                validator: (value){
                  if(value!.isEmpty){
                    return'Clave requerida';
                  }
                },
              ),
            ),
            TextButton(
              onPressed: () {
                //forgot password screen
              },
              child: const Text('Forgot Password',),
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text('Iniciar'),
                  onPressed: _iniciar,
                    //child: const context('/menuPrincipal');

                    //print(nameController.text);
                    //print(passwordController.text
                )),
            //ESTA NAVEGABILIDAD SI SIRVE
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text('Abre'),
                  onPressed: () {
                    Navigator.pushNamed(context, "/menuPrincipal");
                  },
                  //print(nameController.text);
                  //print(passwordController.text
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text('Does not have account?'),
                TextButton(
                  child: const Text(
                    'Sign in',
                    style: TextStyle(fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/registro');
                  },
                )
              ],
            ),
          ],
        )));
  }
}
