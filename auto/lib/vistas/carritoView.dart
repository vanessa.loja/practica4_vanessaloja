/*import 'dart:collection';
import 'dart:convert';
import 'dart:developer';

import 'package:auto/servicios/facade/modelo/CarritoWS.dart';
import 'package:auto/utiles/Utilidades.dart';
import 'package:auto/vistas/MenuBarP.dart';
import 'package:flutter/material.dart';

/// Flutter code sample for [DataTable].
class carreitoView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _carreitoView();
  }
}

class _carreitoView extends State<carreitoView> {
  //const carreitoView({super.key});
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  double subtotal = 0.0;
  double iva = 0.0;
  double total = 0.0;

  @override
  void initState() {
    super.initState();
    //_listar();
    _sumar();
    //_listar();
    //fetchCartData();
    //total = total;
    //subTotalCart = subTotalCart;
  }

  Future<List<CarritoWS>> _listar() async {
    double total = 0.0;
    List<CarritoWS> lista = [];
    Utilidades u = new Utilidades();
    String datos = await u.getCarrito();
    Map<dynamic, dynamic> mapa = jsonDecode(datos);
    for (Map<dynamic, dynamic> v in mapa.values) {
      CarritoWS c = CarritoWS.fromMap(v);
      lista.add(c);
      total += c.cant * c.pu;
    }
    this.total = total;
    return lista;
  }

  void _sumar() async {
    await _listar();
    setState(() {
      subtotal = (total / 1.12);
      iva = total - subtotal;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Carrito de compras"),
        elevation: 10,
        automaticallyImplyLeading: false,
        actions: const [MenuBarP()],
      ),
      body: Column(
        children: [
          ClipOval(
            child: Image(
              image: NetworkImage(
                  'https://images.vexels.com/media/users/3/200097/isolated/preview/942820836246f08c2d6be20a45a84139-icono-de-carrito-de-compras-carrito-de-compras.png'),
              alignment: Alignment.center,
              width: 100,
              height: 100,
            ),
          ),
          Container(
            child: const Text(
              "Carrito de compras",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontStyle: FontStyle.italic,
                  color: Colors.blue,
                  fontWeight: FontWeight.w500,
                  fontSize: 30),
            ),
            padding: const EdgeInsets.all(20),
          ),
          Flexible(
            child: FutureBuilder(
              key: refreshKey,
              future: _listar(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                      child:
                      Text('El carrito esta cargado o no tiene productos'));
                  //return Center(child: CircularProgressIndicator());
                }
                if (snapshot.hasData) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    verticalDirection: VerticalDirection.down,
                    children: <Widget>[
                      Flexible(
                        child: Container(
                            padding: EdgeInsets.all(5),
                            child: dataBody(snapshot.data)),
                      )
                    ],
                  );
                }

                return Center();
              },
            ),
          ),
          Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: [
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.all(5),
                        child: dataFooter(subtotal, iva, total)),
                  )
                ],
              ))
        ],
      ),
    );
  }
}

SingleChildScrollView dataBody(List<CarritoWS>? listSales) {
  return SingleChildScrollView(
    scrollDirection: Axis.vertical,
    child: DataTable(
      headingRowColor: MaterialStateColor.resolveWith(
            (states) {
          return Colors.blue.shade400;
        },
      ),
      columnSpacing: 12,
      dividerThickness: 5,
      dataRowMaxHeight: 80,
      dataTextStyle:
      const TextStyle(fontStyle: FontStyle.normal, color: Colors.black),
      headingRowHeight: 50,
      headingTextStyle:
      const TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      horizontalMargin: 10,
      showBottomBorder: true,
      showCheckboxColumn: false,
      columns: [
        DataColumn(
            label: Expanded(child: Center(child: Text("cant"))),
            numeric: true,
            tooltip: "Cantidad"),
        DataColumn(
          label: Expanded(child: Center(child: Text("Detalle"))),
          numeric: false,
          tooltip: "Detalle",
        ),
        DataColumn(
          label: Expanded(child: Center(child: Text("PU"))),
          numeric: true,
          tooltip: "Precio unitario",
        ),
        DataColumn(
          label: Expanded(child: Center(child: Text("PT"))),
          numeric: true,
          tooltip: "Precio total",
        ),
      ],
      rows: listSales!
          .map(
            (sale) => DataRow(
            onSelectChanged: (b) {
              log(sale.toString());
            },
            cells: [
              DataCell(
                  Text(sale.cant.toString(), textAlign: TextAlign.center)),
              DataCell(
                Text(sale.desc, textAlign: TextAlign.center),
              ),
              DataCell(
                Text("\$" + sale.pu.toString(),
                    textAlign: TextAlign.center),
              ),
              DataCell(
                Text("\$" + sale.pt.toString(),
                    textAlign: TextAlign.center),
              ),
            ]),
      )
          .toList(),
    ),
  );
}

GridView dataFooter(double subtotal, double iva, double total) {
  return GridView.count(
    crossAxisCount: 2,

    mainAxisSpacing: 10.0,
    crossAxisSpacing: 10.0,
    childAspectRatio: 4.0,

    children: [
      Container(child: Text('SUBTOTAL', style: TextStyle(
          fontStyle: FontStyle.italic,
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 15)),alignment: Alignment.centerRight),
      Container(child: Text("\$" +subtotal.toStringAsFixed(2)),alignment: Alignment.centerRight),
      Container(child: Text('IVA', style: TextStyle(
          fontStyle: FontStyle.italic,
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 15)),alignment: Alignment.centerRight),
      Container(child: Text("\$" +iva.toStringAsFixed(2)),alignment: Alignment.centerRight),
      Container(child: Text('TOTAL', style: TextStyle(
          fontStyle: FontStyle.italic,
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 15)),alignment: Alignment.centerRight),
      Container(child: Text("\$" +total.toStringAsFixed(2)),alignment: Alignment.centerRight),
    ],
  );
}*/
import 'package:auto/vistas/CreditCardView.dart';
import 'package:flutter/material.dart';

class carritoView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Carrito de Compras'),
      ),
      body: CarritoContenido(),
    );
  }
}

class CarritoContenido extends StatefulWidget {
  @override
  _CarritoContenidoState createState() => _CarritoContenidoState();
}

class _CarritoContenidoState extends State<CarritoContenido> {
  // Aquí puedes manejar la lista de productos seleccionados
  List<Producto> productosSeleccionados = []; // Debes definir la clase Producto

  double calcularTotal() {
    double total = 0;
    for (var producto in productosSeleccionados) {
      total += producto.precio;
    }
    return total;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView.builder(
            itemCount: productosSeleccionados.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(productosSeleccionados[index].nombre),
                subtitle: Text('\$${productosSeleccionados[index].precio.toStringAsFixed(2)}'),
                // Aquí puedes agregar más detalles del producto si es necesario
              );
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Total: \$${calcularTotal().toStringAsFixed(2)}',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CreditCardView(paymentUrl: 'http://192.168.48.108:3007/api/payment-form'),
                    ),
                  );
                },
                child: Text('Pagar'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class Producto {
  final String nombre;
  final double precio;

  Producto({required this.nombre, required this.precio});
}
