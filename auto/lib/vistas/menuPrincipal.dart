import 'package:auto/vistas/carritoView.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Home',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const menuPrincipal(),
    );
  }
}
class DetalleImagen extends StatelessWidget {
  final String imageUrl;

  DetalleImagen({required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detalle de Imagen'),
      ),
      body: Center(
        child: Image.network(imageUrl),
      ),
    );
  }
}
class menuPrincipal extends StatefulWidget {
  const menuPrincipal({super.key});
  @override
  State<menuPrincipal> createState() => _menuPrincipalState();
}

class _menuPrincipalState extends State<menuPrincipal> {
  int _currentIndex = 0; // Índice seleccionado en la barra de navegación

  final List<String> imageUrls = [
    'https://cdn-icons-png.flaticon.com/512/1088/1088537.png?w=360',
    'https://via.placeholder.com/150',
    'https://via.placeholder.com/150',
    // Agrega más URLs de imágenes aquí
  ];

  @override
  Widget build(BuildContext context) {
    Object? parametros = ModalRoute.of(context)?.settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home',
          style: Theme.of(context)
              .textTheme
              .headline6
              ?.copyWith(color: Colors.white),
        ),
        backgroundColor: Colors.teal[800],
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        backgroundColor: Colors.teal[800],
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
            if (index == 1) { // Índice correspondiente al carrito
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => carritoView(),
                ),
              );
            }
          });
        },
        items: [
          BottomNavigationBarItem(
            backgroundColor: Colors.teal[800],
            icon: Icon(Icons.home),
            label: 'Inicio',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'Carrito',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Mi Cuenta',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.exit_to_app),
            label: 'Cerrar Sesión',
          ),
        ],
      ),

    );
  }

  Widget _buildBody() {
    return GridView.builder(
      padding: EdgeInsets.all(8),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 8,
        crossAxisSpacing: 8,
      ),
      itemCount: imageUrls.length,
      itemBuilder: (context, index) {
        String imageUrl = imageUrls[index];

        return itemDashboard(
          imageUrl,
          Theme.of(context).primaryColor,
        );
      },
    );
  }

  Widget itemDashboard(String imageUrl, Color background) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 5),
            color: Theme.of(context).primaryColor.withOpacity(.2),
            spreadRadius: 2,
            blurRadius: 5,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 80, // Ajusta el ancho según tus preferencias
            height: 80, // Ajusta la altura según tus preferencias
            decoration: BoxDecoration(
              color: background,
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                image: NetworkImage(imageUrl),
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(height: 8),
          Text(
            'Car Image',
            style: Theme.of(context).textTheme.headline6,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
