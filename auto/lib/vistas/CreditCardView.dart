import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
class CreditCardView extends StatelessWidget {
  final String paymentUrl; // La URL del formulario de pago generada en tu backend

  CreditCardView({required this.paymentUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Formulario de Pago'),
      ),
      body: WebView(
        initialUrl: paymentUrl,
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
