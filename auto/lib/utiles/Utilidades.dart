import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class Utilidades {
  final _storage = const FlutterSecureStorage();

  Future<String?> getValue(valor) async {
    return await _storage.read(key: valor);
  }

  void saveValue(llave, valor) async {
    return await _storage.write(key: llave, value: valor);
  }

  static getCarrito(llave, valor) async {
    return 0;
  }

  void removeAllValue() async {
    await _storage.deleteAll();
  }
}